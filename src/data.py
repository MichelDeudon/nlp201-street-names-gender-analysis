import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import plotly
import plotly.express as px
import typing
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

from unidecode import unidecode

# Edit Types de voie (avenue, boulevard... square)
types_de_voie = [
    "allee",
    "avenue",
    "boulevard",
    "carrefour",
    "chemin",
    "cite",
    "clos",
    "cour",
    "croix",
    "esplanade",
    "enclos",
    "espace",
    "faubourg",  # axes et type voie
    "impasse",
    "lotissement",
    "mas",
    "parvis",
    "passage",
    "passerelle",
    "place",
    "plan",
    "rond-point",
    "route",
    "rue",
    "ruelle",
    "square",
    "voie",  # "via", # axes et type voie
    "jardin",
    "jardins",
    "parc",  # espaces verts
    "bassin",
    "bois",
    "fontaine",
    "puit",
    "puits",
    "quai",  # espaces bleus
    "parking",
    "pont",
    "tunnel",  # ingénierie
    "chateau",
    "ecole",
    "ecoles",
    "moulin",
    "palais",
    "tour",  # batiments
    # "colline", "loge",
]

# Domain specific stopwords (not relevant for prediction)
stopwords = set(
    [
        "aux",
        "de",
        "des",
        "del",
        "dit",
        "du",
        "en",
        "et",
        "la",
        "las",
        "le",
        "les",  # mots de liaison
        "petit",
        "petite",  # environement
        "blanc",
        "rouge",
        "roses",
        "vieille",
        "bon",  # couleurs
        "deux",
        "quatre",  # chiffres
        "montpellier",
        "castelnau",
        "ville",  # villes
        "cheval",
        "merle",  # animaux
        "belle",
        "val",
        "mai",  # prenoms from data.gouv.fr (ambigu)
    ]
)
stopwords = set(types_de_voie + list(stopwords))

# Métiers-statuts-rôles
# statuts = ["saint", "sainte", "abbé", "père", "pasteur", "frères", "soeurs", # statut religieux
#          "comte", "comtesse", "roi", "reine", # statut social
#          "lieutenant", "capitaine", "commandant", "colonel", "général", "maréchal", # statut militaire
#          "professeur", "docteur", # autres professions
#          ]

# liste de prénoms féminins ambigu de data.gouv.fr (noms de fleurs, d'abres, de dates comme Victoire du 8 mai)
# fleurs = ["acacia", "lila", "fleur", "rose", "saule", "violette", "hortensia",
#          "magnolia", "eglantine", "camelia", "iris", "victoire"]

# Codes postaux en France Métropole + Outre Mer
cp_france = ["0" * (2 - len(str(i))) + str(i) for i in range(1, 96) if i != 20] + [
    "2A",
    "2B",
    "971",
    "972",
    "973",
    "974",
    "975",
    "976",
    "978",
    "987",
    "988",
]


def clean_str(text: str):
    """
    Args:
        text: str, a raw string to clean
    Returns:
        text: str, a processed clean string
    """

    # lower case and strip text
    text = str(text).lower().strip()

    # remove parenthesis
    idx = text.find("(")
    if idx > 0:
        text = text[:idx].strip()

    # convert accents and special caracters
    text = unidecode(text)
    return text


def get_gendered_names(
    url: str = "https://static.data.gouv.fr/resources/liste-de-prenoms/20141127-154433/Prenoms.csv",
):
    """
    Get liste de prénoms et genres de data.gouv.fr
    Args:
        url: str, url from data.gouv.fr (gendered names dataset)
    Returns:
        names: Dict, a mapping from names to gender ('f' or 'm')
    """

    # Load liste de prénom & genre de data.gouv.fr
    names = pd.read_csv(
        url, encoding="latin", sep=";", usecols=["01_prenom", "02_genre"]
    )
    names = names.sort_values("02_genre", ascending=False)

    # Clean dataset names
    names["01_prenom"] = names["01_prenom"].apply(clean_str)
    names = names.drop_duplicates(subset=["01_prenom"], keep="first")
    names = names.set_index("01_prenom").to_dict()[
        "02_genre"
    ]  # mapping from name to gender, ex: anne -> 'f' (or 'm', 'm,f')

    clean_names = {}
    for name, gender in names.items():
        if name not in stopwords:
            clean_names[name] = gender

    # Add extra names (women and men not in data.gouv.fr list)
    for w in ["simone"]:
        clean_names[w] = "f"
    for w in ["antonin", "fabre", "guilhem"]:
        clean_names[w] = "m"
    return clean_names


names = get_gendered_names()


def label_street(name: str, min_len: int = 3):
    """
    Args:
        name: str, a street name
    Returns:
        label: int, 0 (no gender), 1 (M), 2 (F), 1.5 (uncertain or mixed H/F)
    """

    # iterate over each street words and return 1 (resp 2) if M (resp F) pattern for first name (prenom A)
    for w in name.lower().split():
        w = unidecode(w.strip())  # remove extra white spaces and convert accents
        if w in names and len(w) > min_len:
            gender = names[w]
            if gender == "f":
                return 2
            elif gender == "m":
                return 1
    return 0  # no label


def load_dataset(cp):
    """
    Load BAN dataset for a subset of postal codes
    Args:
        cp: List[str], a list of French postal codes with 2-3 digits (ex: [34, 75, 2A, 974])
    Returns:
        df: pd.DataFrame, a table containing street names as rows (along with lat, long, label)
    """

    data = []
    for c in cp:
        assert str(c) in cp_france
        url = "https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/adresses-{}.csv.gz".format(
            c
        )  # BAN URL (latest)

        # read dataset and drop duplicate rows (group by addresses --> average latitudes and longitudes)
        df = pd.read_csv(
            url,
            sep=";",
            compression="gzip",
            on_bad_lines="skip",
            usecols=[
                "nom_voie",
                "code_postal",
                "code_insee",
                "nom_commune",
                "lon",
                "lat",
            ],
        )
        df = df.groupby(
            by=["code_postal", "code_insee", "nom_commune", "nom_voie"]
        ).mean()
        data.append(df)

    # concatenate files and reset index
    df = pd.concat(data, axis=0)
    df = df.reset_index()

    # clean nom voie (lower case words, convert accents, remove parenthesis, digits and punctuation)
    df["nom_voie_clean"] = df["nom_voie"].apply(clean_str)
    for d in "0123456789-'’\"%&*,./:?":
        df["nom_voie_clean"] = df["nom_voie_clean"].str.replace(d, " ", regex=True)

    # Label French street names
    df["label"] = df["nom_voie_clean"].apply(label_street)
    df["F"] = 0
    df["M"] = 0
    df.loc[df["label"].astype(int) == 2, "F"] = 1
    df.loc[df["label"].astype(int) == 1, "M"] = 1
    return df


def get_stats_topk(vocab, k: int = 100):
    """
    Args:
        vocab: List[str], list of words
        k: int, top k statistics
    Returns:
        ratio_topk: float, 0 (M), 1 (F), 0.5 (mixed)
    """

    sorted_labels = [label_street(w) for w in vocab if w.lower() in names]
    sorted_labels = [l for l in sorted_labels if l == 1.0 or l == 2.0][:k]
    ratio_topk = 100 * (np.mean(sorted_labels) - 1)
    return ratio_topk


def get_stats_saintes(counts_dict):
    """
    Args:
        counts_dict: Dict, mapping from words to counts or frequencies
    Returns:
        ratio_ste: float, 0 (M), 1 (F), 0.5 (mixed)
    """

    try:
        n_occurence_ste = counts_dict["sainte"]  # ex: 'Sainte Genevieve'
    except:
        n_occurence_ste = 0
    n_occurence_st = counts_dict["saint"]  # ex: 'Saint Pierre'
    ratio_ste = 100 * n_occurence_ste / (n_occurence_st + n_occurence_ste)
    # print("{:.0f}% Saintes ({} Ste, {} St)".format(ratio_ste, n_occurence_ste, n_occurence_st))
    return ratio_ste


def get_stats(df):
    """
    Args:
        df: pd.DataFrame
    Returns:
        top50, top100, p_voies, p_saintes: tuple of floats (represenation F/H)
    """

    # get vocabulary and word counts
    corpus = df["nom_voie_clean"].values
    vocab, counts, counts_dict = get_sorted_vocab(corpus)

    # top k indicator
    top50 = get_stats_topk(vocab, k=50)
    top100 = get_stats_topk(vocab, k=100)

    # proportion voies saint/saintes -- Solution simple pour calculer le biais. Limite: High variance.
    p_saintes = get_stats_saintes(counts_dict)

    # proportions voies F/H
    p_voies = (
        100 * len(df[df["F"] == 1]) / (len(df[df["M"] == 1]) + len(df[df["F"] == 1]))
    )
    return top50, top100, p_voies, p_saintes


def get_sorted_vocab(corpus):
    """
    Args:
        corpus: list of str
    Returns:
        vocab: list of str, words in corpus
        counts: list of int, word counts
        counts_dict: Dict, mapping of words and counts
    """

    # save word counts as a dictionnary
    counts_dict = {}
    for name in corpus:
        for w in name.split():
            w = w.lower().strip()
            if len(w) > 0:
                if w in counts_dict:
                    counts_dict[w] += 1
                else:
                    counts_dict[w] = 1

    # sort vocabulary by counts
    names = np.array(list(counts_dict.keys()))
    counts = np.array(list(counts_dict.values()))
    sorted_idx = np.argsort(counts)[::-1]
    return names[sorted_idx], counts[sorted_idx], counts_dict


def plot_word_distr(vocab, counts, offset: int = 0, limit: int = 50):
    """
    Plot Zipf law, long tail distribution and bias (color blue M, red W, gray No Label)
    Args:
        vocab: List[str], list of words
        vocab: List[int], list of word counts or frequency
    """

    fig, ax = plt.subplots()  # figsize=(5,6))

    words_H = [w.capitalize() for w in vocab if label_street(w) == 1.0][
        offset : offset + limit
    ]
    words_F = [w.capitalize() for w in vocab if label_street(w) == 2.0][
        offset : offset + limit
    ]
    counts_H = [-c for (c, w) in zip(counts, vocab) if label_street(w) == 1.0][
        offset : offset + limit
    ]
    counts_F = [c for (c, w) in zip(counts, vocab) if label_street(w) == 2.0][
        offset : offset + limit
    ]

    plt.scatter(counts_F, -np.arange(limit), c="red", label="F", marker="+")
    plt.scatter(counts_H, -np.arange(limit), c="blue", label="H", marker="_")
    plt.plot([0] * limit, -np.arange(limit), linestyle="dotted", c="gray")

    # Rename axis
    plt.yticks(-np.arange(limit), words_H)
    secax = ax.secondary_yaxis(1.0)
    secax.set_yticks(-np.arange(limit))
    secax.set_yticklabels(words_F)
    secax.spines["right"].set_visible(False)

    # Remove ticks/values
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
    ax.set(frame_on=False)
    ax.spines["right"].set_visible(False)

    plt.xlabel("Nombre de voies par genre et prénoms")
    plt.xlim(np.min(counts_H) * 1.01, -np.min(counts_H) * 1.01)
    plt.legend()
    # plt.title("Prénoms les plus populaires dans la dénomination des voies en France.")
    plt.tight_layout()

    # Save figure
    plt.savefig("../img/word_pyramid.png")
    plt.show()


def plot_map(df, zoom=4, title="Métropole", aggregate=False, save_html=False):
    """
    Plots a representation F/H des noms de voies
    Args:
        df: pd.DataFrame
        zoom: int, zoom level
        title: str, map name
        aggregate: bool, if True aggregates F/H counts by cities/postal codes
    """

    hover_data = {"lat": ":.3f", "lon": ":.3f"}
    color = "label"

    if aggregate is True:
        hover_data["label"] = ":.3f"
        hover_data["code_postal"] = True
        hover_name = "nom_commune"
        color_continuous_scale = "bluered"
        color_discrete_map = None
    else:
        df.loc[df["label"] == 2, "label"] = "F"
        df.loc[df["label"] == 1, "label"] = "M"
        df.loc[df["label"] == 0, "label"] = "None"

        hover_data["label"] = True
        hover_data["nom_voie"] = True
        hover_name = "nom_voie"
        color_continuous_scale = None
        color_discrete_map = {
            "None": "#000000",  # black
            "M": "#0000FF",  # blue
            "F": "#FF0000",  # red
        }

    fig = px.scatter_mapbox(
        df,
        lat="lat",
        lon="lon",
        zoom=zoom,
        color=color,
        color_continuous_scale=color_continuous_scale,
        color_discrete_map=color_discrete_map,
        size_max=10,
        opacity=0.5,  # size="type_de_voie",
        hover_name=hover_name,
        hover_data=hover_data,
        mapbox_style="carto-positron",  # no token required for 'open-street-map', 'carto-positron'
        title=title,
        height=750,  # figure height in pixel
    )
    if save_html:
        plotly.offline.plot(fig, filename="../img/map-{}.html".format(title))
    # fig.write_image('../img/map-{}.png'.format(title))
    fig.show()


def preprocess(string: str):
    for w in types_de_voie:
        string = string.replace(w + " ", "")
    return string.strip()


def get_heroines(df: pd.DataFrame, c: str):
    """
    Get local heroines using contrastive analysis (common within dpt vs. ext)
    Args:
        df: pd.DataFrame, dataset
        c: str, postal code
    Returns:
        results: List[str], list of heroines' names
    """

    if len(c) == 1:
        c = c + "00"
    elif len(c) == 2:
        c = c + "0"

    dff = df[df["F"] == 1]
    local = dff[dff["code_postal"].astype(str).str[:3] == c].nom_voie_clean.unique()
    local = [preprocess(w) for w in local]
    ext = dff[dff["code_postal"].astype(str).str[:3] != c].nom_voie_clean.unique()
    ext = [preprocess(w) for w in ext]

    results = []
    for voie in local:
        if voie not in ext and len(voie.split()) >= 2 and voie.split()[0] in names:
            heroine = " ".join([w.capitalize() for w in voie.split()])
            results.append(heroine)
    results = list(set(results))  # remove duplicates
    results = np.sort(results)  # sort by name
    return results
