import os
import sys


def test_import():
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    import plotly
    import plotly.express as px
    import typing
    import ssl
    from unidecode import unidecode


def test_domain_knowledge(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    from src.data import types_de_voie, stopwords, cp_france

    assert "avenue" in types_de_voie
    assert "du" in stopwords
    assert "2B" in cp_france


def test_clean_str(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    from src.data import clean_str

    text = "Allée Sophie Germain (Batiment B)"
    new_text = clean_str(text)
    assert new_text == "allee sophie germain"


def test_get_gendered_names(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    # read dataset (prenoms et genre from data.gouv.fr)
    from src.data import get_gendered_names

    names = get_gendered_names()
    assert len(names) > 0
    assert "marie" in names
    assert names["jean"] == "m"
    assert names["jules"] == "m"
    assert names["maryam"] == "f"


'''
def test_load_street_names(c="34", app_prefix="."):
    """
    Args:
        c: str, a postal code
    """
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    # read dataset (BAN street names)
    from src.data import load_dataset

    df = load_dataset(cp=[c])
    assert len(df) > 0
    assert "lat" in df
    assert "lon" in df
    assert "nom_voie" in df
'''


def test_label_street(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    from src.data import label_street

    assert label_street("Allée des Platanes") == 0
    assert label_street("Avenue Paul Valéry") == 1
    assert label_street("Square Edith Piaf") == 2


def test_get_stats_topk(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    from src.data import get_stats_topk

    vocab = [
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Marie",
    ]
    topk = get_stats_topk(vocab, k=10)
    assert int(topk) == 10  # percent


def test_get_stats_saintes(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    from src.data import get_stats_saintes

    counts_dict = {"sainte": 1, "saint": 9}
    p_sainte = get_stats_saintes(counts_dict)
    assert int(p_sainte) == 10  # percent


def test_get_sorted_vocab(app_prefix="."):
    sys.path.insert(0, os.path.dirname(app_prefix))
    sys.path.insert(0, os.path.dirname(os.path.join(app_prefix, "src/")))

    from src.data import get_sorted_vocab

    corpus = [
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Michel",
        "Marie",
    ]
    names, counts, counts_dict = get_sorted_vocab(corpus)
    assert "michel" in names and len(names) == 2
    assert counts[0] == 9
    assert counts_dict["michel"] == 9
    assert counts_dict["marie"] == 1
