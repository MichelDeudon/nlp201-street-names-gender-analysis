# NLP201 Street Names - Gender Analysis
NLP201 Frugal Innovation &amp; Computation Linguistics. Unit #9. Debias Language

# Contexte et objectif

La dénomination des voies publiques en France et sur [data.gouv.fr](https://adresse.data.gouv.fr/) quantifient des siècles de stéréotypes de genre, comme les représentations linguistiques apprises sur Wikipedia (Garg et al, 2018). En effet, les rues portant des noms de femmes sont en faible proportion par rapport aux rues portant des noms d'hommes (Mapbox, 2015). Or, les dénominations des rues ont pour fonction de valoriser des événements et des personnages historiques marquants en les commémorant (Oto-Peralias, 2016). Les dénominations des rues reflètent donc des valeurs sociales et des choix politiques qui renvoient des messages qui pourraient être interprétés comme un projet (in)conscient d’invisibilité des femmes et de leurs réalisations dans l’espace public.

Dans ce contexte, l'objectif de ce projet est d'utiliser de simples méthodes de linguistique computationnelle pour estimer et <b>quantifier le taux de représentation des femmes dans la dénomination des rues et espaces publics</b> en France. Pour cela, nous nous appuyons sur les données de la [Base Adresse Nationale](https://adresse.data.gouv.fr/) (BAN, 2022), reconnue par l’administration et placée sous la responsabilité de la Première ministre, ainsi qu'une liste de +10k prénoms et genres en accès-libre sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/liste-de-prenoms/).

Ce projet s'inscrit dans une démarche citoyenne et d'innovation sociale pour les acteurs publiques, élus locaux, communes et associations. Il s'appuye sur [Matplotlib](https://matplotlib.org/), [Seaborn](https://seaborn.pydata.org/) et [Plotly](https://plotly.com/) pour la visualisation des données et l'interprétation des résultats.

# Installation

Cloner ce projet avec `git clone`.

Créer un environnement Python 3.8+, par exemple avec Anaconda `conda create -n miashs python=3.8`.

Activer l'environment avec `activate miashs` et installer les dépendances avec `pip install -r requirements.txt`.

Lancer jupyter notebook avec `jupyter notebook` et executer le notebook `application.ipynb` pour reproduire les résultats.

# Résultats

## 1. Analyse spatiale

Le [jeu de données](https://adresse.data.gouv.fr/) original représente plus de 2 millions de voies en France, dont 14% contiennent un prénom de la liste de prénoms et genre de [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/liste-de-prenoms/). La figure ci-dessous montre localement les voies de Montpellier contenant un prénom genré. Chaque voie est représentée par un point bleu (nom masculin), rouge (nom féminin) ou gris (autre). Le taux de féminisation des voies publiques à Montpellier est compris entre 12 et 16% en janvier 2024.

![map-Montpellier-min](img/map-Montpellier-min.png)

Nous annotons les noms de voie de la [Base Adresse Nationale](https://adresse.data.gouv.fr/) (BAN, 2022) avec un label (F, H ou autre), à partir des prénoms identifiés d’un [jeu de données de prénoms et genres](https://www.data.gouv.fr/fr/datasets/liste-de-prenoms/) (data.gouv.fr, 2014). Nous pré-processons les noms de voie (lettres en minuscules, sans chiffres et ponctuation) avant d'itérer sur chaque mot constituant un nom de voie pour en extraire les prénoms et genres. Par exemple "rue Sainte Anne" contient le prénom "Anne", nous l'annotons avec le label F. La voie "av. Paul Valéry" contient "Paul", nous l'annotons avec le label H. Dans les autres cas, nous renvoyons le label autre.

Les données agrégées par communes en France métropolitaine (noms de voies et genre) sont illustrées dans la figure ci-dessous. Pour chaque commune, nous calculons un indicateur compris entre 0 (bleu) et 1 (rouge), et qui correspond à la part de prénoms féminins dans la dénomination des voies contenant un prénom, féminin ou masculin (la parité correspond à un label égal à 0.5). Le taux de féminisation des voies et espaces publiques à l'échelle nationale se situe entre 10 et 16%.

![map-Métropole-min](img/map-Métropole-min.png)

Nous reportons dans la table ci-dessous cet indicateur (% Voies) calculé à l'échelle nationale et pour les 10 plus grandes villes françaises en janvier 2024, et comparons nos résultats à d'autres indicateurs comme la proportion de voies contenant le mot "Sainte" versus "Saint" (% Saintes), ou la proportion de prénoms féminins parmi les k plus populaires (Top k prénoms). Nos résultats soulignent les disproportionalités entre dénominations d'espaces publics et genre, globalement et avec des disparités locales.

| Zone géographique | Top 50 prénoms |Top 100 prénoms | % Voies | % Saintes | Moyenne |
|----------|:--------:|---:|---:|---:| ------:|
| France |  10 | 14| 15.8 | 12 | 13 |
| Paris |  12 | 18| 17.3 | 13.2 | 15.1 |
| Marseille |  6 | 19| 15.4 | 26.8 | 16.8 |
| Lyon |  12 | 21| 15.1 | 10.2 | 14.6 |
| Toulouse |  14 | 17| 15.4 | 19.1 | 16.4 |
| Nice |  14 | 20| 18.6 | 18.2 | 17.7 |
| Nantes |  14 | 19| 22.3 | 11.1 | 16.6 |
| Montpellier |  6 | 14| 15 | 15.8 | 12.7 |
| Strasbourg |  14 | 23| 20.9 | 26.9 | 21.2 |
| Bordeaux |  10 | 18| 14.1 | 31.2 | 18.3 |
| Lille |  12| 18| 14.3 | 15.5 | 14.9 |

## 2. Analyse linguistique

La distribution des prénoms suit la loi de Zipf (Zipf, 1949): une minorité de prénoms (ici à 90% masculins) apparaissent très fréquemment tandis que la plupart des prénoms apparaissent peu fréquemment.
Parmi les 50 prénoms les plus courants dans les noms de voies, 5 sont féminins: Marie, Blanche, Jeanne, Anne et Iris. La distribution des prénoms est biaisée vers une représentation masculine de l'histoire et de ses représentants et héros, comme illustrée dans la Figure 3. Nous notons que certains prénoms féminins du [jeu de données de data.gouv.fr](https://www.data.gouv.fr/fr/datasets/liste-de-prenoms/) renvoient en réalité à des noms de fleurs et de plantes (Iris, Rose, Saule, Fleur, etc) ou à des dates (Victoire du 8 mai), et non pas à des personnes historiques. Ces prénoms concernent 8 à 9% des voies labellisées féminin avec notre approche de traitement automatique du langage. Ils peuvent conduire à surestimer la part des femmes dans la Base Adresse Nationale (BAN, 2022) et montrent les limites de notre méthode.

![word_pyramid-min](img/word_pyramid-min.png)

## 3. Analyse par type de voie

Autre résultat intéressant, la représentation des femmes varie d'un type de voie à un autre. On observe que les jardins, les parcs, les fontaines, les chemins et les ruelles sont connotés à des divinités de la Nature, à des noms de fleurs et prénoms féminins, alors que les avenues et les boulevards sont à dominante masculine (à plus de 90%) et connotés à des chefs de guerre. Les tunnels quant à eux sont exclusivement masculins.

| Type de voie | Voies avec le label H | Voies avec le label F | Représentativité F/H |
|----------|:--------:|---:| ------:|
| Voie |  520 | 428 | 45% |
| Tour |  7 | 4 | 36% |
| Fontaine |  24 | 11 | 31% |
| Puits |  7 | 3 | 30% |
| Parc |  101 | 31 | 24% |
| Chemin |  10223 | 3110 | 23% |
| Ruelle |  911 | 257 | 22% |
| Allée |  13182 | 3505 | 21% |
| Impasse |  18875 | 4785 | 20% |
| Passerelle |  12 | 3 | 20% |
| Passage |  1129 | 264 | 19% |
| Jardin |  20 | 4 | 17% |
| Toute voie confondue |  20 | 4 | 15% |
| Chateau |  72 | 11 | 13% |
| Pont |  114 | 17 | 13% |
| Rue |  152946 | 22102 | 13% |
| Esplanade |  220 | 31 | 12% |
| Place |  14542 | 1721 | 11% |
| Avenue |  13943 | 1551 | 10% |
| Boulevard |  3593 | 279 | 7% |
| Tunnel |  2 | 0 | 0% |

## 4. Analyse par type de profession

Nous reportons dans la table suivante les taux de représentation F/H associées à cinq professions.

| Profession | Voies avec le label H | Voies avec le label F | Représentativité F/H |
|----------|:--------:|---:| ------:|
| Docteur |  1615 | 70| 4.2% |
| Capitaine |  238 | 8 | 3.3% |
| Lieutenant |  227 | 4 | 1.7% |
| Professeur |  258 | 4 | 1.5% |
| Colonel |  499 | 4 | 0.8% |

Avec ces biais de genre, un modèle statistique naïf doit générer plus de 25 prénoms de docteurs (Albert Tomey, Paul Pezet, Albert Schweitzer, Robert Koch...) en moyenne pour obtenir un prénom féminin. Des exemples de ces biais peuvent être obtenus en écrivant "rue du docteur..." sur Google Maps ou en se promenant, curieux, la tête levée.

# Contribuer

Installer pre-commit avec `pip install pre-commit` puis `pre-commit install` pour contribuer au code.

Parmi les directions de recherches futures, il pourrait être intéressant de quantifier et lutter contre d'autres formes de discriminations (religion, couleur, orientation sexuelle, âge, nationalité, handicap, apparence physique, statut socio-économique), ou encore d'analyser d'autres pays et distributions (noms des écoles, des monuments, etc.).

Aussi, afin de sensibiliser les élus locaux et citoyens sur le sujet, et faire évoluer le taux de représentation des femmes dans la dénomination des rues et espaces publics, il pourrait être intéréssant de proposer un outil pour les communes (problème des doublons après fusion, recommandations locales) et des jeux à destination du grand public, comme ce [jeu de mémory](https://www.mtpcours.fr/u/Jeux-Memory-Montpellier-qui-est-ce.pdf) qui rend hommages à des femmes et héroïnes de l'Hérault (Occitanie).

# Licence

[![CC BY 4.0][cc-by-shield]][cc-by]

Le code Python et le vocabulaire utilisé sont en accès libre sur Framagit, sous licence [Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

# Remerciements

Nous remercions les communes, la DINUM, l'ANCT et l'IGN pour les données en accès libre sur [data.gouv.fr](https://www.data.gouv.fr/fr/), ainsi qu'Arnaud Sallaberry, Sandra Bringuay et Jana Thompson pour leurs discussions.
